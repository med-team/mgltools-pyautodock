############################################################################
#
# Authors: William Lindstrom, Ruth Huey
#
# Copyright: A. Olson TSRI 2007
#
#############################################################################

#
# $Id: test_shaperecognition.py,v 1.2.10.1 2016/02/12 08:01:40 annao Exp $
#


import unittest
import math
from PyAutoDock.Tests.test_scorer import ScorerTest, WeightedDataBase
from PyAutoDock.shaperecognition import ShapeRecognition


class ShapeRecognitionTest(ScorerTest):

    def setUp(self):
        self.setup_ind_ind() # set up MolecularSystem

    def test_init(self):
        """instantiate the ShapeRecognition class."""
        usr = ShapeRecognition()
        usr.set_molecular_system(self.ms)
        self.assertEqual(usr.__class__, ShapeRecognition)


class ShapeRecognitionIdentityTest(ScorerTest):
    def setUp(self):
        self.setup_ind_ind() # set up MolecularSystem

    def test_identity(self):
        """The ind_ind molecular system should have score = 1.0
        since the molecule coords are the same
        """
        usr = ShapeRecognition()
        usr.set_molecular_system(self.ms)
        print usr.get_score()
        self.assertAlmostEqual(1.0, usr.get_score())


class ShapeRecognitionOtherTest(ScorerTest):
    def setUp(self):
        self.setup_crn_ind() # set up MolecularSystem

    def test_nonidentity(self):
        """The crn_ind molecular system should NOT have score = 1.0
        since the molecule coords are NOT the same
        """
        usr = ShapeRecognition()
        usr.set_molecular_system(self.ms)
        print usr.get_score()
        self.assertNotAlmostEqual(1.0, usr.get_score())


# ShapeRecognitionTest




if __name__ == '__main__':

    test_cases = [
        'ShapeRecognitionTest',
        'ShapeRecognitionIdentityTest',
        'ShapeRecognitionOtherTest',
        ]
    
    unittest.main( argv=([__name__ ,'-v'] + test_cases) )

